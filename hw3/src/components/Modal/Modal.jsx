import React from 'react';
import './Modal.scss';

const Modal = ({newModalContent, closeModalWindow, buyProductWithFavorites, buyProductWithHome, currentByProduct, handleRemoveFromCart}) =>{



        return (
            <>
                <div className="modal-overlay" onClick={closeModalWindow}></div>
                <div className="modal">
                    <div className="modal-header">
                        <div className="modal-content">{newModalContent.title}</div>
                        <button className="close-button" onClick={closeModalWindow}>X</button>
                        <div className="modal-actions">{newModalContent.subtitle}</div>
                    </div>
                    <button className="confirm-button" onClick={()=>{

                    if(newModalContent.operation==='Buy with favorites'){
                        buyProductWithFavorites(currentByProduct)
                        closeModalWindow()
                    }

                    if (newModalContent.operation==='Buy'){
                        buyProductWithHome(currentByProduct)
                        closeModalWindow()
                    }

                    if(newModalContent.operation==='Remove'){
                            handleRemoveFromCart(currentByProduct)
                            closeModalWindow()
                    }

                    }

                    }>Ok</button>
                    <button className="cancel-button" onClick={closeModalWindow}>Cancel</button>
                </div>
            </>
        );
}

export default Modal;

