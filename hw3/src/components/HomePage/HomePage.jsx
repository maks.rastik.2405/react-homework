import React from 'react';
import ProductCard from '../ProductCard/ProductCard';
import './HomePage.scss';

const HomePage = ({
                      products,
                      favorites,
                      cartItems,
                      handleOpenModal,
                      handleToggleFavorite,
                      handleRemoveFromCart,
                      handleRemoveFromFavorites,
                      addToCurrentCard,
                      openModalWindow,
                      changeModalContent
                  }) => {
    return (
        <div className="product-list">
            {products.map((product) => (
                <ProductCard
                    key={product.id}
                    product={product}
                    favorites={favorites}
                    cartItems={cartItems}
                    onToggleFavorite={handleToggleFavorite}
                    openModalWindow={openModalWindow}
                    changeModalContent={changeModalContent}
                    addToCurrentCard={addToCurrentCard}
                    handleOpenModal={(modalContent) => handleOpenModal(product, modalContent)}
                    isFavorite={favorites.includes(product)}
                    inCart={false}
                    handleRemoveFromCart={handleRemoveFromCart}
                    handleRemoveFromFavorites={handleRemoveFromFavorites}
                />
            ))}
        </div>
    );
};

export default HomePage;
