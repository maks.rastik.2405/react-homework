import React from 'react';
import { ReactComponent as StarEmpty } from '../../SVG/star.svg';
import { ReactComponent as StarFilled } from '../../SVG/star (1).svg';
import './ProductCard.scss';

const ProductCard = ({
                         product,
                         favorites,
                         cartItems,
                         onToggleFavorite,
                         openModalWindow,
                         addToCurrentCard,
                         changeModalContent
                     }) => {

    const isFavorite = (card) => favorites.find(el => el.id === card.id);
    const isBasket = (card) => cartItems.find(el => el.id === card.id);

    const modalContent = {
        title: 'Are you sure?',
        subtitle: `In what you want to buy ${product.name}`,
        operation: 'Buy',
    };

    const handleToggleFavorite = () => {
        onToggleFavorite(product);
    };

    return (
        <div className="product-card">
            <img className="product-image" src={product.image} alt={product.name} />
            <div className="product-title">{product.name}</div>
            <div className="product-price">Price: {product.price}$</div>
            <div>Type of Engine: {product.engine}</div>
            <div>Horsepower: {product.hp}</div>
            <div>Year: {product.years}</div>
            <div className="product-id">Id: {product.id}</div>
            <div className="product-actions">
                {!isBasket(product) && (
                    <>
                        <button className="cart-button" onClick={()=>{
                        openModalWindow()
                        addToCurrentCard(product)
                        changeModalContent(modalContent)
                        }
                        }>
                            Add to Cart
                        </button>
                    </>
                )}
                <div className="star-icon">
                    {!isFavorite(product) && <StarEmpty onClick={handleToggleFavorite} width={16} height={16}/>}
                    {isFavorite(product) && <StarFilled onClick={handleToggleFavorite} width={16} height={16}/>}
                </div>
            </div>
        </div>
    );
};

export default ProductCard;
