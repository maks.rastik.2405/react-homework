import React from "react";


const CartCard = ({
                      product,
                      showRemoveIcon,
                      openModalWindow,
                      changeModalContent,
                      addToCurrentCard
                  }) => {
    const cartModalContent  = {
            title: 'Are you sure?',
            subtitle: `Do you want to remove ${product.name} from your cart?`,
            operation: 'Remove',

    };
    console.log(product,'product')
return(
    <div className="product-card">
        <img className="product-image" src={product.image} alt={product.name} />
        <div className="product-title">{product.name}</div>
        <div className="product-price">Price: {product.price}$</div>
        <div>Type of Engine: {product.engine}</div>
        <div>Horsepower: {product.hp}</div>
        <div>Year: {product.years}</div>
        <div className="product-id">Id: {product.id}</div>
        <div className="product-actions">
            {showRemoveIcon && (
                <button className="remove-icon" onClick={() => {
                openModalWindow()
                addToCurrentCard(product)
                changeModalContent(cartModalContent)
                }
                }>
                    X
                </button>
            )}
        </div>
    </div>
)
}

export default CartCard