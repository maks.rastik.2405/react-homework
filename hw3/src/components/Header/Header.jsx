import React from 'react';
import { Link } from 'react-router-dom';
import ShoppingCartIcon from "../ShoppingCartIcon/ShoppingCartIcon";
import FavoritesIcon from "../FavoritesIcon/FavoritesIcon";
import './Header.scss';

const Header = ({ cartItems, favorites }) => {
    return (
        <header>
            <Link to="/" className='header-logo'>
                <h1>AutoStock</h1>
            </Link>
            <div className="header-icons">
                <div className="flex-cart">
                    <Link to="/shopping-cart">
                        <img className='icon' src='https://cdn.onlinewebfonts.com/svg/img_277709.png' alt='cart-icon' />
                    </Link>
                    <ShoppingCartIcon itemCount={cartItems.length} />
                </div>
                <div className='flex-star'>
                    <Link to="/favorites">
                        <img className='icon' src='https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Noun_Project_Star_icon_370530_cc.svg/1045px-Noun_Project_Star_icon_370530_cc.svg.png' alt='star-icon' />
                    </Link>
                    <FavoritesIcon itemCount={favorites.length} />
                </div>
            </div>
        </header>
    );
};

export default Header;
