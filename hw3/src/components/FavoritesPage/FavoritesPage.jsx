import React from 'react';
import FavoritesCard from '../FavoritesCard/FavoritesCard';
import {ReactComponent as SadSmile} from "../../SVG/sad-svgrepo-com.svg";
import './FavoritesPage.scss'

const FavoritesPage = ({ favorites, cartItems,openModalWindow,  changeModalContent, addToCurrentCard, handleRemoveFromFavorites, currentByProduct, }) => {




    return (
        <div>
            <h1 className='header-components'>Favorites</h1>
            {favorites.length === 0 ? (
                <h2 className='header-empty'>Your favorites list is empty.
                    <SadSmile className='svg-icon' width={150} height={150}/>
                </h2>
            ) : (
                <div className="product-list">
                    {favorites.map((product) => (
                        <FavoritesCard
                            key={product.id}
                            product={product}
                            cartItems={cartItems}
                            isFavorite={favorites.includes(product)}
                            openModalWindow={openModalWindow}
                            handleRemoveFromFavorites={handleRemoveFromFavorites}
                            changeModalContent={changeModalContent}
                            addToCurrentCard={addToCurrentCard}
                            currentByProduct={currentByProduct}
                        />
                    ))}
                </div>
            )}
        </div>
    );
};

export default FavoritesPage;
