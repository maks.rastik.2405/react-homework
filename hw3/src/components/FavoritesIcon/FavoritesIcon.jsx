import React from 'react';
import PropTypes from 'prop-types';

const FavoritesIcon = ({ itemCount }) => {
    return (
        <div className="favorites-icon">
            <i className="fas fa-heart"></i>
            <span>{itemCount}</span>
        </div>
    );
};

FavoritesIcon.propTypes = {
    itemCount: PropTypes.number.isRequired,
};

export default FavoritesIcon;
