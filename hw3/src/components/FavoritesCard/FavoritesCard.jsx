import React from 'react';
import { ReactComponent as StarEmpty } from '../../SVG/star.svg';
import { ReactComponent as StarFilled } from '../../SVG/star (1).svg';

const FavoritesCard = ({
                           product,
                           cartItems,
                           isFavorite,
                           handleRemoveFromFavorites,
                           openModalWindow,
                           changeModalContent,
                           addToCurrentCard,
                       }) => {

    const isBasket = (card) => cartItems.find(el => el.id === card.id);

    const modalContentFavorites = {
        title: 'Are you sure ?',
        subtitle:`Are you sure you want to remove from favorites,and add to cart?`,
        operation: 'Buy with favorites'
    }

    return (
        <div className="product-card">
            <img className="product-image" src={product.image} alt={product.name} />
            <div className="product-title">{product.name}</div>
            <div className="product-price">Price: {product.price}$</div>
            <div>Type of Engine: {product.engine}</div>
            <div>Horsepower: {product.hp}</div>
            <div>Year: {product.years}</div>
            <div className="product-actions">
                {!isBasket(product) && (
                    <>
                        <button className="cart-button" onClick={()=>{
                            openModalWindow()
                            addToCurrentCard(product)
                            changeModalContent(modalContentFavorites)
                        }
                        }>
                            Add to Cart
                        </button>
                    </>
                )}
                <button className="star-icon" onClick={() => handleRemoveFromFavorites(product)}>
                    {isFavorite ? <StarFilled /> : <StarEmpty width={16} height={16} />}
                </button>
            </div>
        </div>
    );
};

export default FavoritesCard;
