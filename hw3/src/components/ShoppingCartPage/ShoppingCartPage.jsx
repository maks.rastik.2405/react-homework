import React from "react";
import CartCard from "../CartCard/CartCard";
import {ReactComponent as SadSmile} from "../../SVG/sad-svgrepo-com.svg";

const ShoppingCartPage = ({ cartItems, openModalWindow, changeModalContent, addToCurrentCard }) => {

    return (

        <div>
            <h1 className='header-components'>Cart</h1>
            {cartItems.length === 0 ? (
                <h2 className='header-empty'>Your cart is empty.
                    <SadSmile className='svg-icon' width={150} height={150}/>
                </h2>
            ) : (
                <div className="product-list">
                    {cartItems.map((product) => (
                        <CartCard
                            key={product.id}
                            product={product}
                            openModalWindow={openModalWindow}
                            changeModalContent={changeModalContent}
                            addToCurrentCard={addToCurrentCard}
                            showRemoveIcon={true}
                        />
                    ))}
                </div>
            )}
        </div>
    )
};

export default ShoppingCartPage;

