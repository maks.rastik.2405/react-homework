import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import HomePage from "./components/HomePage/HomePage";
import Header from "./components/Header/Header";
import ShoppingCartPage from './components/ShoppingCartPage/ShoppingCartPage';
import FavoritesPage from './components/FavoritesPage/FavoritesPage';
import './App.scss';
import Modal from "./components/Modal/Modal";
import axios from "axios";

function App() {
    const [products, setProducts] = useState([]);
    const [cartItems, setCartItems] = useState([]);
    const [favorites, setFavorites] = useState([]);
    const [openModal,setOpenModal] = useState(false);
    const [newModalContent, setNewModalContent] = useState({});
    const [currentByProduct, setCurrentByProduct] = useState({});

    useEffect(() => {
        axios.get('/product.json')
            .then(response => {
                setProducts(response.data);
            })
            .catch(error => {
                console.error(error);
            });
    }, []);


    useEffect(() => {
        const storedCartItems = JSON.parse(localStorage.getItem('cartItem')) || [];
        setCartItems(storedCartItems);

        const storedFavorites = JSON.parse(localStorage.getItem('favorites')) || [];
        setFavorites(storedFavorites);
    }, []);


    const openModalWindow = () =>{
        setOpenModal(true)
    }

    const closeModalWindow = () => {
        setOpenModal(false)
    }

    const changeModalContent = (content) => {
        setNewModalContent(content)
    }

    const buyProductWithHome = (product) => {

        const updatedCartItems = [...cartItems, product];
        localStorage.setItem('cartItem', JSON.stringify(updatedCartItems));
        setCartItems(updatedCartItems);
    }

    const buyProductWithFavorites = (currentByProduct) => {
        const updatedCartItems = [...cartItems, currentByProduct];
        localStorage.setItem('cartItem', JSON.stringify(updatedCartItems));
        setCartItems(updatedCartItems);
        const updatedFavorites = favorites.filter((item) => item.id !== currentByProduct.id);
        setFavorites(updatedFavorites);
        localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
    }

    const addToCurrentCard = (card) => {
        setCurrentByProduct(card)
    }

    const addFavorites = (products) => {
        const isFavorite = favorites.includes(products);

        let updatedFavorites;
        if (isFavorite) {
            updatedFavorites = favorites.filter((fav) => fav !== products);
        } else {
            updatedFavorites = [...favorites, products];
        }

        setFavorites(updatedFavorites);
        localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
    };

    const handleRemoveFromCart = (item)=>{
        setCartItems(cartItems.filter(el => el.id!==item.id));
        localStorage.setItem("basket", JSON.stringify(cartItems.filter(el => el.id!==item.id)))
    }
    const handleRemoveFromFavorites = (product) => {
        const updatedFavorites = favorites.filter((item) => item.id !== product.id);
        setFavorites(updatedFavorites);
        localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
    };

    return (
        <div>
            <Router>
                <Header
                        cartItems={cartItems}
                        favorites={favorites}
                />
                <Routes>
                    <Route
                        path="/shopping-cart"
                        element={<ShoppingCartPage
                            cartItems={cartItems}
                            openModalWindow={openModalWindow}
                            changeModalContent={changeModalContent}
                            addToCurrentCard={addToCurrentCard}
                            handleRemoveFromCart={handleRemoveFromCart}
                        />}
                    />
                    <Route
                        path="/favorites"
                        element={<FavoritesPage
                            favorites={favorites}
                            cartItems={cartItems}
                            handleRemoveFromFavorites={handleRemoveFromFavorites}
                            openModalWindow={openModalWindow}
                            changeModalContent={changeModalContent}
                            addToCurrentCard={addToCurrentCard}
                        />}
                    />
                    <Route
                        path="/"
                        element={
                            <HomePage
                                products={products}
                                cartItems={cartItems}
                                favorites={favorites}
                                openModalWindow={openModalWindow}
                                changeModalContent={changeModalContent}
                                addToCurrentCard={addToCurrentCard}
                                currentByProduct={currentByProduct}
                                handleToggleFavorite={addFavorites}
                                handleRemoveFromCart={handleRemoveFromCart}
                                handleRemoveFromFavorites={handleRemoveFromFavorites}
                            />
                        }
                    />
                </Routes>
            </Router>
            {
                 openModal &&
                <Modal
                    newModalContent={newModalContent}
                    closeModalWindow={closeModalWindow}
                    buyProductWithHome={buyProductWithHome}
                    buyProductWithFavorites={buyProductWithFavorites}
                    currentByProduct={currentByProduct}
                    handleRemoveFromCart={handleRemoveFromCart}
                />
            }
        </div>
    );
}

export default App;

