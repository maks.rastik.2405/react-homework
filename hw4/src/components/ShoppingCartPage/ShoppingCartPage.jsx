import React from "react";
import CartCard from "../CartCard/CartCard";
import {connect} from "react-redux";

import {ReactComponent as SadSmile} from "../../SVG/sad-svgrepo-com.svg";

const ShoppingCartPage = ({ cartItems}) => {

    return (

        <div>
            <h1 className='header-components'>Cart</h1>
            {cartItems.length === 0 ? (
                <h2 className='header-empty'>Your cart is empty.
                    <SadSmile className='svg-icon' width={150} height={150}/>
                </h2>
            ) : (
                <div className="product-list">
                    {cartItems.map((product) => (
                        <CartCard
                            key={product.id}
                            product={product}
                            showRemoveIcon={true}

                        />
                    ))}
                </div>
            )}
        </div>
    )
};

const mapStateCart =({cartItems})=>({cartItems:cartItems})

export default connect(mapStateCart)(ShoppingCartPage);

