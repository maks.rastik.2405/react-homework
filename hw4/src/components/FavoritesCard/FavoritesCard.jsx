import React from 'react';
import {connect} from "react-redux";
import {removeFavoritesOperation} from "../../store/favorites/operations";
import {saveCurrentCardOperation} from "../../store/card/operations";
import {openModalOperation} from "../../store/modal/operations";
import { ReactComponent as StarFilled } from '../../SVG/star (1).svg';
import {contentBuyFavoriteOperation} from "../../store/modalContent/operation";
const FavoritesCard = ({
                           product,
                           cartItems,
                           isFavorite,
                           openModalWindow,
                           hangleContentModalBayFav,
                           addToCurrentCard,
                           removeFromFavorites
                       }) => {

    const isBasket = (card) => cartItems.find(el => el.id === card.id);


    return (
        <div className="product-card">
            <img className="product-image" src={product.image} alt={product.name} />
            <div className="product-title">{product.name}</div>
            <div className="product-price">Price: {product.price}$</div>
            <div>Type of Engine: {product.engine}</div>
            <div>Horsepower: {product.hp}</div>
            <div>Year: {product.years}</div>
            <div className="product-actions">
                {!isBasket(product) && (
                    <>
                        <button className="cart-button" onClick={()=>{
                            openModalWindow()
                            addToCurrentCard(product)
                             hangleContentModalBayFav(product)

                        }
                        }>
                            Add to Cart
                        </button>
                    </>
                )}
                <button className="star-icon" onClick={() => removeFromFavorites(product)}>
                    {!isFavorite && <StarFilled  width={16} height={16}/> }
                </button>
            </div>
        </div>
    );
};

const mapStateToProps = ({ cartItems}) => ({cartItems:cartItems});
const mapDispatchToProps = (dispatch) => ({
    removeFromFavorites: (card) => dispatch(removeFavoritesOperation(card)),
    addToCurrentCard: (card) => dispatch(saveCurrentCardOperation(card)),
    openModalWindow: () => dispatch (openModalOperation()),
    hangleContentModalBayFav : (card)=> dispatch(contentBuyFavoriteOperation(card))
})

export default connect(mapStateToProps, mapDispatchToProps)(FavoritesCard);
