import React from 'react';
import { connect } from 'react-redux';
import { addFavoriteListOperation, removeFavoritesOperation} from "../../store/favorites/operations";
import { openModalOperation } from "../../store/modal/operations";
import {contentBuyOperation} from "../../store/modalContent/operation";
import {saveCurrentCardOperation} from "../../store/card/operations";
import { ReactComponent as StarEmpty } from '../../SVG/star.svg';
import { ReactComponent as StarFilled } from '../../SVG/star (1).svg';
import './ProductCard.scss';


const ProductCard = ({product, favorite, cartItems, removeFromFavorites, addFavorite, changeContentModalBay, openModalWindow, addToCurrentCard}) => {
    const isFavorite = (card) => favorite.find(el => el.id === card.id);
    const isBasket = (card) => cartItems.find(el => el.id === card.id);

    return (
        <div className="product-card">
            <img className="product-image" src={product.image} alt={product.name} />
            <div className="product-title">{product.name}</div>
            <div className="product-price">Price: {product.price}$</div>
            <div>Type of Engine: {product.engine}</div>
            <div>Horsepower: {product.hp}</div>
            <div>Year: {product.years}</div>
            <div>Color: {product.color}</div>
            <div className="product-id">Id: {product.id}</div>


            <div className="product-actions">
                {!isBasket(product) && (
                    <>
                        <button className="cart-button" onClick={()=> {
                            addToCurrentCard(product)
                            changeContentModalBay(product)
                            openModalWindow()
                        }} >
                            Add to Cart
                        </button>
                    </>
                )}
                <div className="star-icon">
                    {!isFavorite(product) && <StarEmpty onClick={()=>addFavorite(product)}  width={16} height={16} />}
                    {isFavorite(product) && <StarFilled onClick = {()=>{removeFromFavorites(product)}}  width={16} height={16} />}
                </div>
            </div>
        </div>
    );
};

const mapStateProduct = ({favorite , cart, cartItems}) => ({favorite , cart, cartItems});

const mapDispatchProduct = (dispatch) => ({
    addFavorite : (card) => dispatch(addFavoriteListOperation(card)),
    removeFromFavorites : (card) => dispatch(removeFavoritesOperation(card)),
    changeModalContent : (card) => dispatch(contentBuyOperation(card)),
    openModalWindow : () => dispatch(openModalOperation()),
    addToCurrentCard : (card) => dispatch(saveCurrentCardOperation(card)),
    changeContentModalBay : (card)=> dispatch(contentBuyOperation(card))
});

export default connect(mapStateProduct, mapDispatchProduct)(ProductCard);
