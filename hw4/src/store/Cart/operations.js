import {getCartList, addToCartList, removeToCartList } from './actions'


export const getCartListOperation = (basketList) => (dispatch) => {
    dispatch (getCartList(basketList))
}

export const addToCartListOperation = (card) => (dispatch) =>{
    dispatch(addToCartList(card))
}

export const removeToCartListOperation = (card) => (dispatch) =>{
    dispatch(removeToCartList(card))
}
