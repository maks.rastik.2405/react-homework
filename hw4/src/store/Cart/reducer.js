import * as cartAction from './types'

const reducer = (state=[], action) =>{
    switch (action.type){
        case cartAction.GET_CART_LIST:
            return action.payload
        case cartAction.ADD_CART_LIST:
            console.log(action.payload);
            localStorage.setItem("basketList", JSON.stringify([...state, action.payload]))
            return [...state, action.payload]
        case cartAction.REMOVE_CART_LIST:
            localStorage.setItem("basketList", JSON.stringify(state.filter(el => el.id!==action.payload.id)))
            return state.filter(el => el.id!==action.payload.id)
        default:
            return state

    }
}

export default reducer