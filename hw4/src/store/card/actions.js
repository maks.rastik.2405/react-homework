import * as cardsAction from './types'

export const saveCards =(cards)=>({
    type:cardsAction.SAVE_CARDS,
    payload:cards
})

export const  saveCurrentCard = (card) =>({
    type:cardsAction.SAVE_CURRENT_CARD,
    payload:card
})