import * as modalActions from './types';


export const openModal = ()=> ({
    type: modalActions.OPEN_MODAL,
})

export const closeModal =()=>({
    type:modalActions.CLOSE_MODAL,
})
