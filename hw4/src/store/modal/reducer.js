import * as modalActions from './types';

const initialState = false;


const reducer = (state = initialState, action) => {
    switch (action.type){
        case modalActions.OPEN_MODAL:
            return true;

        case modalActions.CLOSE_MODAL:
            return  false;

        default:
            return state;

    }
}

export default reducer;
