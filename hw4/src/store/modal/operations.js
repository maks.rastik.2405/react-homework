import { closeModal, openModal} from "./actions";

export const openModalOperation = () => (dispatch) => {
    dispatch (openModal());
};

export const closeModalOperation = () => (dispatch) => {
    dispatch (closeModal());
};
