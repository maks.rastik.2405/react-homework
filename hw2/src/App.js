import React from 'react';
import MainPage from './components/MainPage/MainPages';
import './App.scss'

function App() {
    return (
        <div className="App">
            <MainPage />
        </div>
    );
}

export default App;
