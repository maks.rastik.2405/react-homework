import React from 'react';
import ShoppingCartIcon from "../ShoppingCartIcon/ShoppingCartIcon";
import FavoritesIcon from "../FavoritesIcon/FavoritesIcon";
import './Header.scss'

const Header = ({  cartItems, favorites }) => {
    return (
        <header>
            <h1>
              AutoStock
            </h1>
            <div className="header-icons">
                <div className="flex-cart">
                <img className='icon' src='https://cdn.onlinewebfonts.com/svg/img_277709.png' alt='cart-icon'/>
                <ShoppingCartIcon itemCount={cartItems.length}/>
                </div>
                <div className='flex-star'>
                <img className='icon' src='https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Noun_Project_Star_icon_370530_cc.svg/1045px-Noun_Project_Star_icon_370530_cc.svg.png' alt='star-icon'/>
                <FavoritesIcon itemCount={favorites.length}/>
                </div>
            </div>
        </header>
    );
};

export default Header;