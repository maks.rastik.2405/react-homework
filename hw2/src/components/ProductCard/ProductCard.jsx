import React, {useState} from 'react';
import {ReactComponent as StarEmpty} from '../../SVG/star.svg'
import {ReactComponent as StarFilled} from '../../SVG/star (1).svg'
import './ProductCard.scss'


const ProductCard = ({ product, onAddToCart, onToggleFavorite, handleOpenModal }) => {
    const [isFavorite, setIsFavorite] = useState(false);

    const modalContent = {
        title: 'Are you sure?',
        subtitle: `In what you want to buy ${product.name}`,
        operation: 'Buy'
    };

    const handleToggleFavorite = () => {
        setIsFavorite(!isFavorite);
        onToggleFavorite(product);
    };

    return (
        <div className="product-card">
            <img className="product-image" src={product.image} alt={product.name} />
            <div className="product-title">{product.name}</div>
            <div className="product-price">Price:{product.price}$</div>
            <div>Type of Engine:{product.engine}</div>
            <div>HoursePower:{product.hp}</div>
            <div>Year:{product.years}</div>
            <div className="product-id">Id:{product.id}</div>
            <div className="product-actions">
                <button className="cart-button" onClick={()=>handleOpenModal(product,modalContent)}>Add to Cart</button>
                <button className="star-icon" onClick={handleToggleFavorite}>
                        {isFavorite ? <StarFilled /> : <StarEmpty width={16} height={16} />}
                </button>
            </div>
        </div>
    );
};

export default ProductCard;



