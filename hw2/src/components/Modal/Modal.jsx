import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Modal.scss';

class Modal extends Component {
    static propTypes = {
        onClose: PropTypes.func.isRequired,
        text: PropTypes.string.isRequired,
    };

    render() {
        const {  text , handleCloseModal , modalContent , modalOperation} = this.props;
        console.log(modalContent)
        return (
            <>
                <div className="modal-overlay" onClick={()=>handleCloseModal()}></div>
                <div className="modal">
                    <div className="modal-header">
                        <div className="modal-content">{text.title}</div>
                        <button className="close-button" onClick={()=>handleCloseModal()}>X</button>
                        <div className="modal-actions">{text.subtitle}</div>
                    </div>
                    <button className="confirm-button" onClick={()=>modalOperation()}>Ok</button>
                    <button className="cancel-button" onClick={()=>handleCloseModal()}>Cancel</button>
                </div>

            </>

        );
    }
}

export default Modal;

