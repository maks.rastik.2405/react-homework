import React from 'react';
import PropTypes from 'prop-types';

const ShoppingCartIcon = ({ itemCount }) => {
    return (
        <div className="shopping-cart-icon">
            <i className="fas fa-shopping-cart"></i>
            <span>{itemCount}</span>
        </div>
    );
};

ShoppingCartIcon.propTypes = {
    itemCount: PropTypes.number.isRequired,
};

export default ShoppingCartIcon;
