import { PureComponent } from 'react';
import axios from 'axios';
import Modal from '../Modal/Modal';
import Header from "../Header/Header";
import HomePage from "../HomePage/HomePage";
import './MainPages.scss';

class MainPage extends PureComponent {
    state = {
        products: [],
        cartItems: [],
        favorites: [],
        showModal: false,
        modalText: {},
        currentByProduct:{},
    };

    componentDidMount() {
        axios.get('/product.json')
            .then(response => {
                this.setState({ products: response.data });
            })
            .catch(error => {
                console.error(error);
            });
    }

    modalOperation = (product) =>{
        if (this.state.modalText.operation === 'Buy') {
            const updatedCartItems = [...this.state.cartItems, this.state.currentByProduct];
            localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
            this.setState({cartItems: updatedCartItems});
        }
        this.setState({ showModal: false });
    };

    handleOpenModal=(product,modalContent)=>{
        this.setState({showModal:true})
        this.setState({modalText:modalContent})
        this.setState({currentByProduct:product})
    };

    handleCloseModal = () => {
        console.log('Close Modal')
        this.setState({ showModal: false });
    };


     handleToggleFavorite = (product) => {
         const { favorites } = this.state;
         const isFavorite = favorites.includes(product);

         let updatedFavorites;
         if (isFavorite) {
             updatedFavorites = favorites.filter((fav) => fav !== product);
         } else {
             updatedFavorites = [...favorites, product];
         }

        this.setState({ favorites: updatedFavorites });

        localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
     };



    render() {
        console.log(this.state.modalText)
        return (
        <div>

            <Header
                favorites={this.state.favorites}
                cartItems={this.state.cartItems}
            />

            <HomePage
                products={this.state.products}
                favorites={this.state.favorites}
                handleOpenModal={this.handleOpenModal}
                handleToggleFavorite={this.handleToggleFavorite}
            />

            {
                this.state.showModal &&
                <Modal
                    text={this.state.modalText}
                    handleCloseModal={this.handleCloseModal}
                    modalOperation={this.modalOperation}
                />
            }

        </div>

        );
    }
}

export default MainPage;
