import React from 'react';
import ProductCard from "../ProductCard/ProductCard";
import './HomePage.scss'


const HomePage = ({ products, favorites ,handleOpenModal,modalOperation , handleToggleFavorite }) => {
    console.log(products);
    return (
        <div className="product-list">
            {products.map((product) => (
                <ProductCard
                    key={product.id}
                    product={product}
                    handleOpenModal={handleOpenModal}
                    modalOperation={modalOperation}
                    onToggleFavorite={handleToggleFavorite}
                    isFavorite={favorites.includes(product)}
                />
            ))}
        </div>


    );
};

export default HomePage;
