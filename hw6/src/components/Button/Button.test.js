import Button from './Button';
import {render, screen, fireEvent} from "@testing-library/react";

describe('buttonTest',  ()=>{
    it('smokeTestButton', ()=>{
        render(<Button text='text'/>)
    })
    it('renderButtonTest', ()=> {
        render(<Button text='text'/>)
        const btn = screen.getByText('text')
        expect(btn).toBeDefined()
    })
    it('handleOnClickBtnTest', ()=> {
        const handleClick = jest.fn()
        render(<Button text='text' onClick={handleClick()}/>)
        const btn = screen.getByText('text')
        expect(btn).toBeDefined()
        fireEvent.click(btn)
        expect(handleClick).toHaveBeenCalledTimes(1)
    })
    it('snapshot test button', () => {
        render(<Button text='text'/>)
        const btn = screen.getByText('text')
        expect(btn).toMatchSnapshot();
    });
})