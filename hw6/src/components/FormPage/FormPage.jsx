import React from 'react';
import {Form, Formik} from "formik";
import validationSchema from './validationSchema'
import Input from '../Input/Input';
import {connect} from "react-redux";
import {cleanUpToCartList} from "../../store/Cart/actions";
import {useNavigate} from "react-router-dom";
import './FormPage.scss'



const FormPage = ({basketList, cleanBasket}) => {
    const navigate = useNavigate();
    return (
        <div className='formBlock'>
            <p className='formTitle'>Order form</p>
            <Formik
                initialValues={{
                    firstName:'',
                    lastName:'',
                    age:'',
                    address:'',
                    tel:'',
                }}

                validationSchema={validationSchema}
                onSubmit= {(values) => {
                    console.log(values, 'FormValue');
                    console.log(basketList, 'goods');
                    cleanBasket();
                    navigate(-1)
                }
                }>

                {({ errors, touched }) => (
                    <Form className='formWrapper'>
                        <Input
                            name='firstName'
                            placeholder ='Enter first name'
                            isError={errors.firstName && touched.firstName}
                            errorText={errors.firstName}
                        />

                        <Input
                            name='lastName'
                            placeholder ='Enter last name'
                            isError={errors.lastName && touched.lastName}
                            errorText={errors.lastName}
                        />

                        <Input
                            name='age'
                            placeholder ='Enter age'
                            isError={errors.age && touched.age}
                            errorText={errors.age}
                        />

                        <Input
                            name='address'
                            placeholder ='Enter address'
                            isError={errors.address && touched.address}
                            errorText={errors.address}
                        />

                        <Input
                            name='tel'
                            placeholder ='Enter tel'
                            isError={errors.tel && touched.tel}
                            errorText={errors.tel}
                        />

                        <button type="submit" className='confirm-buy'>Confirm Buy</button>
                    </Form>
                )}
            </Formik>
        </div>
    );
};

const mapStateToProps =({basket})=>({basketList:basket});

const mapDispatchToProps = (dispatch) => ({
    cleanBasket:() => dispatch(cleanUpToCartList())
})

export default connect(mapStateToProps, mapDispatchToProps) (FormPage);