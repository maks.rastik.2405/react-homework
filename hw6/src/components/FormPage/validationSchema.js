import * as Yup from "yup";

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
const nameRegExp =/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/

const validationSchema = Yup.object().shape({
    firstName: Yup.string().matches(nameRegExp, 'Name is not valid')
        .min(3, 'The name field must be longer than 2 characters')
        .max(30, 'The name field must be less than 30 characters')
        .required('The first name is required'),
    lastName:Yup.string().matches(nameRegExp, 'Last name is not valid')
        .min(3, 'The last name field must be longer than 2 characters')
        .max(30, 'The last name field must be less than 30 characters')
        .required('The last name is required'),
    age:Yup.number()
        .min(18, 'The age must be more than 17')
        .max(100, 'The age must be less than 101')
        .required('The age is required'),
    tel:Yup.string().matches(phoneRegExp, 'Phone number is not valid')
        .min(8, 'The tel field must be longer than 10 characters')
        .max(15, 'The tel field must be less than 15 characters')
        .required('The phone number is required'),
    address:Yup.string()
        .min(5, 'The address must be longer than 5 characters')
        .max(50, 'The address must be less than 50 characters')
        .required('The address is required'),
});

export default validationSchema;