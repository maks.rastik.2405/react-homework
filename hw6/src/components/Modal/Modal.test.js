import {Modal} from './Modal'
import {render, screen} from "@testing-library/react";


const defaultProps= {
    title:"testTitle",
    subtitle :"testSubtitle"
};

describe("testModalWindow", ()=>{
    it("modalWindowSmokeTest", () => {
        render(<Modal modalContent={defaultProps}/> )
    })
    it("modalRenderTest", () => {
        render(<Modal modalContent={defaultProps}/>)
        const title = screen.getByText('testTitle')
        const subtitle = screen.getByText('testSubtitle')
        expect(title).toBeDefined()
        expect(subtitle).toBeDefined()
    })
    it("modalRenderTest", () => {
        render(<Modal modalContent={defaultProps}/>)
        const title = screen.getByText('testTitle')
        expect(title).toMatchSnapshot();
    })
})
