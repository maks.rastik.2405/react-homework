import React from 'react';
import FavoritesCard from '../FavoritesCard/FavoritesCard';
import {ReactComponent as SadSmile} from "../../SVG/sad-svgrepo-com.svg";
import {connect} from "react-redux";
import {removeFavoritesOperation} from "../../store/favorites/operations";
import {openModalOperation} from "../../store/modal/operations";
import {contentBuyFavoriteOperation} from "../../store/modalContent/operation";
import {saveCurrentCardOperation} from "../../store/card/operations";
import './FavoritesPage.scss'
const FavoritesPage = ({favorite}) => {
    console.log(favorite);


    return (
        <div>
            <h1 className='header-components'>Favorites</h1>
            {favorite.length === 0 ? (
                <h2 className='header-empty'>Your favorites list is empty.
                    <SadSmile className='svg-icon' width={150} height={150}/>
                </h2>
            ) : (
                <div className="product-list">
                    {favorite.map((product) => (<FavoritesCard key={product.id} product={product}/>
                    ))}
                </div>
            )}
        </div>

    );
};
const mapStateToProps = ({favorite}) => ({favorite:favorite});

const   mapDispatchToProps = (dispatch) => ({

    openModal : () => dispatch(openModalOperation()),
    removeFavorites : (card) => dispatch(removeFavoritesOperation(card)),
    changeModalBuyFavorites : (card) => dispatch(contentBuyFavoriteOperation(card)),
    addToCurrentCard : (card) => dispatch(saveCurrentCardOperation(card))

})
export default connect(mapStateToProps,  mapDispatchToProps)(FavoritesPage);
