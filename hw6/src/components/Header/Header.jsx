import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import './Header.scss';

const Header = ({ cartItems, favorite }) => {


    return (
        <header>
            <Link to="/" className='header-logo'>
                <h1>AutoStock</h1>
            </Link>
            <div className="header-icons">
                <div className="flex-icons">
                    <div className="icon-with-count">
                        <Link to="/shopping-cart">
                            <img className="icon" src="https://cdn.onlinewebfonts.com/svg/img_277709.png" alt="cart-icon" />
                        </Link>
                        <p className="item-count">{cartItems.length}</p>
                    </div>
                    <div className="icon-with-count">
                        <Link to="/favorites">
                            <img
                                className="icon"
                                src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Noun_Project_Star_icon_370530_cc.svg/1045px-Noun_Project_Star_icon_370530_cc.svg.png"
                                alt="star-icon"
                            />
                        </Link>
                        <p className="item-count">{favorite.length}</p>
                    </div>
                </div>
            </div>
        </header>
    );
};

const mapStateToProps = ({cartItems, favorite}) => ({cartItems:cartItems, favorite:favorite})

export default connect(mapStateToProps) (Header);
