import React from 'react';
import {Field} from "formik";
import "./Input.scss";

const Input = ({name, placeholder, isError, errorText}) => (
    <div className='container'>
        <Field className='input' name={name} placeholder={placeholder} />
        {isError ? (<p className='errorText'>{`*${errorText}`}</p>) : null}
    </div>
);

export default Input;