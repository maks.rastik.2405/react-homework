import * as favoriteAction from './types';


const reducer =(state=[], action)=> {
    switch (action.type) {
        case favoriteAction.GET_FAVORITE_LIST:
            return action.payload
        case favoriteAction.ADD_TO_FAVORITE_LIST:
            localStorage.setItem("favorite", JSON.stringify([...state, action.payload]))
            return [...state, action.payload]
        case favoriteAction.REMOVE_TO_FAVORITE_LIST:
            localStorage.setItem("favorite", JSON.stringify(state.filter(item => item.id !== action.payload.id)))
            return state.filter(item => item.id !== action.payload.id)
        default:
            return state
    }

}

export default reducer