import {saveFavorite, addToFavorites, removeToFavorites} from './actions'


export const saveFavoriteListOperation = (favoriteList) => (dispatch) => {
    dispatch(saveFavorite(favoriteList))
}

export const addFavoriteListOperation = (card) => (dispatch) => {
    dispatch(addToFavorites(card))
}

export const removeFavoritesOperation = (card) =>(dispatch) =>{
    dispatch(removeToFavorites(card))
}