import reducer from './reducer';
import * as favoriteAction from './types';

describe('Test favorite reducer', () => {
    it('should return the initial state', () => {
        const initialState = [];
        const action = {};
        const newState = reducer(initialState, action);
        expect(newState).toEqual(initialState);
    });

    it(' get the favorite list', () => {
        const favoriteList = [{ id: 59121259, name: 'Porsche Cayenne' }, { id: 97671977, name: 'Volkswagen Touareg' }];
        const initialState = [];
        const action = {
            type: favoriteAction.GET_FAVORITE_LIST,
            payload: favoriteList,
        };
        const newState = reducer(initialState, action);
        expect(newState).toEqual(favoriteList);
    });

    it(' add to the favorite list', () => {
        const initialState = [{ id: 59121259, name: 'Porsche Cayenne' }];
        const newItem = { id: 97671977, name: 'Volkswagen Touareg' };
        const action = {
            type: favoriteAction.ADD_TO_FAVORITE_LIST,
            payload: newItem,
        };
        const newState = reducer(initialState, action);
        expect(newState).toEqual([...initialState, newItem]);
    });

    it(' remove from the favorite list', () => {
        const initialState = [{ id: 59121259, name: 'Porsche Cayenne' }, { id: 97671977, name: 'Volkswagen Touareg' }];
        const itemIdToRemove = 59121259;
        const action = {
            type: favoriteAction.REMOVE_TO_FAVORITE_LIST,
            payload: { id: itemIdToRemove },
        };
        const newState = reducer(initialState, action);
        expect(newState).toEqual([{ id: 97671977, name: 'Volkswagen Touareg' }]);
    });
});
