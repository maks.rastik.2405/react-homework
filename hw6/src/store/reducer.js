import {combineReducers} from "redux";
import cards from './card/reducer'
import favorite from './favorites/reducer'
import cartItems from './Cart/reducer'
import modal from './modal/reducer'
import modalContent from './modalContent/reducer'


const rootReducer = combineReducers({
  cards,
  favorite,
  cartItems,
  modal,
  modalContent
})
export default rootReducer