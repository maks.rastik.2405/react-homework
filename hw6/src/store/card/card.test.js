import reducer from "./reducer";
import * as cardsAction from './types'


describe('testSaveCardReducer ', ()=>{

    it('return initial state', ()=>{
        const initialState = {
            cards: [],
            card: null
        };
        const action = {};
        expect(reducer(initialState, action)).toEqual(initialState);
    });

    it('save cards', () => {
        const initialState = {
            cards: [],
            card: null
        };
        const cards = [{ id: 16253289, name: 'Volkswagen Passat' }, { id: 18777772, name: 'Mercedes-Benz G63' }];
        const action = {
            type: cardsAction.SAVE_CARDS,
            payload: cards,
        };
        expect(reducer(initialState, action).cards).toEqual(cards);
    });

    it('save current card', ()=>{
        const initialState = {
            cards: [],
            card: null,
        };
        const currentCard = { id: 18764532, name: 'Audi S8' };
        const action = {
            type: cardsAction.SAVE_CURRENT_CARD,
            payload: currentCard,
        };
        expect(reducer(initialState, action).card).toEqual(currentCard);
    });
})