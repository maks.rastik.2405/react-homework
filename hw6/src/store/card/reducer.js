import * as cardsAction from "./types";


const initialState = {
    cards:[]
}

const reducer =(state = initialState, action)=>{
    switch(action.type){
        case cardsAction.SAVE_CARDS:
            return {...state, cards:action.payload}

        case cardsAction.SAVE_CURRENT_CARD:{
            return  {...state, card:action.payload}
        }

        default:
            return state
    }
}

export default reducer
