import * as modalContentType from './types';

const reducer = (state={}, action) => {
    switch (action.type) {
        case modalContentType.CHANGE_MODAL_CONTENT_BUY :
            return {
                title: 'Are you sure?',
                subtitle: `In what you want to buy ${action.payload.name}`,
                operation: 'Buy',
            }
        case modalContentType.CHANGE_MODAL_CONTENT_DELETE :
            return {
                title: 'Are you sure?',
                subtitle: `Do you want to remove ${action.payload.name} from your cart?`,
                operation: 'Remove',
            }
        case modalContentType.CHANGE_MODAL_CONTENT_BUY_FAV :
            return {
                title: 'Are you sure ?',
                subtitle:`Are you sure you want to remove from favorites,and add to cart?`,
                operation: 'Buy with favorites'
            }
        default :
            return  state
    }
}

export default reducer