import reducer from './reducer';
import * as modalContentType from './types';

describe('Test modal content reducer', () => {
    it('should return the initial state', () => {
        const initialState = {};
        const action = {};
        expect(reducer(initialState, action)).toEqual(initialState);
    });

    it('should change modal content for buy action', () => {
        const initialState = {};
        const itemToBuy = { id: 18569012, name: 'Cadillac Escalade' };
        const action = {
            type: modalContentType.CHANGE_MODAL_CONTENT_BUY,
            payload: itemToBuy,
        };
        expect(reducer(initialState, action)).toEqual({
            title: 'Are you sure?',
            subtitle: `In what you want to buy ${itemToBuy.name}`,
            operation: 'Buy',
        });
    });

    it('should change modal content for delete action', () => {
        const initialState = {};
        const itemToDelete = { id: 18569012, name: 'Cadillac Escalade' };
        const action = {
            type: modalContentType.CHANGE_MODAL_CONTENT_DELETE,
            payload: itemToDelete,
        };
        expect(reducer(initialState, action)).toEqual({
            title: 'Are you sure?',
            subtitle: `Do you want to remove ${itemToDelete.name} from your cart?`,
            operation: 'Remove',
        });
    });

    it('should change modal content for buy with favorites action', () => {
        const initialState = {};
        const itemToBuyWithFavorites = { id: 18569012, name: 'Cadillac Escalade' };
        const action = {
            type: modalContentType.CHANGE_MODAL_CONTENT_BUY_FAV,
            payload: itemToBuyWithFavorites,
        };
        expect(reducer(initialState, action)).toEqual({
            title: 'Are you sure ?',
            subtitle: 'Are you sure you want to remove from favorites,and add to cart?',
            operation: 'Buy with favorites'
        });
    });
});
