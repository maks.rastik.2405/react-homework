import reducer from './reducer';
import * as modalType from './types';

describe('Test reducer modal', ()=>{

    it('Incorrect action', ()=>{
        const action = {type:'MODAL'}
        expect( reducer(false, action)).toEqual(false)
    })

    it('Open modal test', ()=>{
        const actionOpen = {type:modalType.OPEN_MODAL};
        expect (reducer(false, actionOpen)).toEqual(true)
    })

    it('Close modal test', ()=>{
        const actionClose = {type: modalType.CLOSE_MODAL}
        expect(reducer(true, actionClose)).toEqual(false)
    })

})