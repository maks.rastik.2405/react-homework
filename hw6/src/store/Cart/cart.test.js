import reducer from "./reducer";
import * as cartAction from './types';

describe('Tests cart reducer', ()=>{

    it('return the initial state', () => {
        const initialState = [];
        const action = {};
        expect(reducer(initialState, action)).toEqual(initialState)
    });

    it('get the cart list', ()=>{
        const initialState = [];
        const cartList = [{id: 58777778, name:'Lamborghini Huracan'},{id: 77777777, name:'Rolls-Royce Ghost  Black Badge'}];
        const action = {
            type : cartAction.GET_CART_LIST,
            payload : cartList
        };
        expect(reducer(initialState,action)).toEqual(cartList)
    });

    it('add to cart list ', ()=>{
        const initialState = [{id: 58777778, name:'Lamborghini Huracan'}];
        const newItem = {id: 77777777, name:'Rolls-Royce Ghost  Black Badge'};
        const action = {
            type: cartAction.ADD_CART_LIST,
            payload: newItem
        }
        expect(reducer(initialState, action)).toEqual([...initialState, newItem])
    })

    it('remove from cart list ', ()=>{
        const initialState = [{id: 58777778, name:'Lamborghini Huracan'},{id: 77777777, name:'Rolls-Royce Ghost  Black Badge'}];
        const  itemIdToRemove = 77777777;
        const action = {
            type : cartAction.REMOVE_CART_LIST,
            payload: { id: itemIdToRemove }
        }
        expect(reducer(initialState, action)).toEqual([{ id: 58777778, name: 'Lamborghini Huracan' }]);
    });

    it('clean the cart list ', ()=>{
        const initialState =  [{ id: 58777778, name: 'Lamborghini Huracan' }];
        const action = {
            type: cartAction.CLEAN_CART_LIST,
        };
        expect(reducer(initialState, action)).toEqual([]);
    });
})