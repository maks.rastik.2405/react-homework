import * as cartAction from './types'

export const getCartList = (cartItems) =>({
    type:cartAction.GET_CART_LIST,
    payload:cartItems
})

export const addToCartList = (card) => ({
    type:cartAction.ADD_CART_LIST,
    payload: card
})

export const removeToCartList = (card) =>({
    type: cartAction.REMOVE_CART_LIST,
    payload: card
})

export const cleanUpToCartList = () =>({
    type:cartAction.CLEAN_CART_LIST
})
