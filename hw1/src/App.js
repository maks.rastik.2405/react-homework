import React from 'react';
import './App.scss';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';

class App extends React.Component {
    state = {
        showModal1: false,
        showModal2: false,
    };

    openModal1 = () => {
        this.setState({ showModal1: true });
    };

    openModal2 = () => {
        this.setState({ showModal2: true });
    };

    closeModal1 = () => {
        this.setState({ showModal1: false });
    };

    closeModal2 = () => {
        this.setState({ showModal2: false });
    };

    render() {
        const { showModal1, showModal2 } = this.state;

        return (
            <div className="app">
                <h1>Modal App</h1>
                <Button
                    backgroundColor="#007bff"
                    text="Open first modal"
                    onClick={this.openModal1}
                />
                <Button
                    backgroundColor="#6c757d"
                    text="Open second modal"
                    onClick={this.openModal2}
                />

                {showModal1 && (
                    <Modal
                        header="Do you want to Delete this File ?"
                        closeButton={true}
                        text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
                        actions={<button className='button-close' onClick={this.closeModal1}>Close</button>}
                        onClose={this.closeModal1}
                    />
                )}

                {showModal2 && (
                    <Modal
                        header="Rayon"
                        closeButton={true}
                        text="Rayon is a semi-synthetic fiber, made from natural sources of regenerated cellulose, such as wood and related agricultural products. It has the same molecular structure as cellulose. It is also called viscose"
                        actions={<button className='button-close' onClick={this.closeModal2}>Close</button>}
                        onClose={this.closeModal2}
                    />
                )}
            </div>
        );
    }
}

export default App;
