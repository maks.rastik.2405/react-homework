import React from 'react';
import './Modal.scss';

class Modal extends React.Component {
    handleClickOutside = (event) => {
        if (event.target === this.modalRef) {
            this.props.onClose();
        }
    };

    render() {
        const { header, closeButton, text, actions } = this.props;

        return (
            <div className="modal-overlay" onClick={this.handleClickOutside}>
                <div className="modal" ref={(ref) => (this.modalRef = ref)}>
                    <div className="modal-header">
                        {header}
                        {closeButton && (
                            <button className="modal-close" onClick={this.props.onClose}>
                                &times;
                            </button>
                        )}
                    </div>
                    <div className="modal-content">{text}</div>
                    <div className="modal-actions">{actions}</div>
                </div>
            </div>
        );
    }
}

export default Modal;
