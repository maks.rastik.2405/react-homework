import React from 'react';
import ProductCard from '../ProductCard/ProductCard';
import {connect} from "react-redux";

import './HomePage.scss';


const HomePage = ({cards,}) => {
    return (
        <div className="product-list">
            { cards.map((product) => (<ProductCard key={product.id} product={product}/>))}
        </div>
    );
};

const mapStateToProps = ({cards}) => ({cards:cards.cards});

const mapDispatchHomePage = (dispatch) => ({


})

export default connect(mapStateToProps, mapDispatchHomePage)(HomePage);
