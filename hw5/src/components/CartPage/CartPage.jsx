import React, {useEffect, useState} from "react";
import CartCard from "../CartCard/CartCard";
import {connect} from "react-redux";
import {ReactComponent as SadSmile} from "../../SVG/sad-svgrepo-com.svg";
import {NavLink} from "react-router-dom";
import style from './CartPage.scss'

const CartPage = ({ cartItems}) => {

    const [totalPrice, setTotalPrice] = useState();

    useEffect(()=>{
        setTotalPrice(cartItems.reduce((accumulator, item)=> accumulator+item.price, 0 ))
        }

    )

    return (

        <div>
            <h1 className='header-components'>Cart</h1>
            {cartItems.length === 0 ? (
                <h2 className='header-empty'>Your cart is empty.
                    <SadSmile className='svg-icon' width={150} height={150}/>
                </h2>
            ) : (
                <div className="product-list">
                    {cartItems.map((product) => (
                        <CartCard
                            key={product.id}
                            product={product}
                            showRemoveIcon={true}

                        />
                    ))}
                </div>
            )}
            {
                cartItems.length>0 && (
                    <div className='totalBlock'>
                        <h2 className='priceBlock'>Total: {totalPrice}$</h2>
                        <NavLink to='/form'><button className='buyButton'>Buy</button></NavLink>
                    </div>
                )
            }
        </div>
    )
};

const mapStateCart =({cartItems})=>({cartItems:cartItems})

export default connect(mapStateCart)(CartPage);

