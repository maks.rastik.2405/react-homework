import React from 'react';
import {connect} from "react-redux";
import  {closeModalOperation} from "../../store/modal/operations";
import './Modal.scss';
import {addToCartListOperation, removeToCartListOperation} from "../../store/Cart/operations";
import {removeFavoritesOperation} from "../../store/favorites/operations";


const Modal = ({modalContent, closeModalWindow, card,addBasket, removeBasket, removeFromFavorites}) =>{
    console.log(card);


    return (
            <>
                <div className="modal-overlay" onClick={()=>closeModalWindow()}></div>
                <div className="modal">
                    <div className="modal-header">
                        <div className="modal-content">{modalContent.title}</div>
                        <button className="close-button" onClick={()=>closeModalWindow()}>X</button>
                        <div className="modal-actions">{modalContent.subtitle}</div>
                    </div>
                    <button className="confirm-button" onClick={()=>{

                    if(modalContent.operation==='Buy with favorites'){
                        addBasket(card)
                        removeFromFavorites(card)
                        closeModalWindow()
                    }

                    if (modalContent.operation==='Buy'){
                        addBasket(card)
                        closeModalWindow()
                    }

                    if(modalContent.operation==='Remove'){
                        removeBasket(card)
                        closeModalWindow()
                     }
                                                                             }
                    }
                    >Ok</button>
                    <button className="cancel-button" onClick={closeModalWindow}>Cancel</button>
                </div>
            </>
        );
}

const mapDispatchToProps = (dispatch) => ({
    closeModalWindow : () => dispatch(closeModalOperation()),
    addBasket : (card) => dispatch(addToCartListOperation(card)),
    removeBasket : (card) => dispatch(removeToCartListOperation(card)),
    removeFromFavorites: (card) => dispatch(removeFavoritesOperation(card)),
})

const mapStateToProps = ({modalContent, cards}) =>({modalContent:modalContent, card:cards.card})

export default connect(mapStateToProps,mapDispatchToProps)(Modal);

