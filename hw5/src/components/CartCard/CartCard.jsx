import React from "react";
import { connect } from "react-redux";
import { saveCurrentCardOperation } from "../../store/card/operations";
import { openModalOperation } from "../../store/modal/operations";
import { contentDeleteOperation } from "../../store/modalContent/operation";

const CartCard = ({
                      product,
                      showRemoveIcon,
                      openModalWindow,
                      changeModalContent,
                      addToCurrentCard
                  }) => {
    console.log(product,'product')
return(
    <div className="product-card">
        <img className="product-image" src={product.image} alt={product.name} />
        <div className="product-title">{product.name}</div>
        <div className="product-price">Price: {product.price}$</div>
        <div>Type of Engine: {product.engine}</div>
        <div>Horsepower: {product.hp}</div>
        <div>Year: {product.years}</div>
        <div className="product-id">Id: {product.id}</div>
        <div className="product-actions">
            {showRemoveIcon && (
                <button className="remove-icon" onClick={() => {
                openModalWindow()
                addToCurrentCard(product)
                changeModalContent(product)
                }
                }>
                    X
                </button>
            )}
        </div>
    </div>
)
};

const mapDispatchCard = (dispatch) => ({
   addToCurrentCard : (card) => dispatch(saveCurrentCardOperation(card)),
   openModalWindow : () => dispatch(openModalOperation()),
    changeModalContent : (card) => dispatch(contentDeleteOperation(card))
})

export default connect(null, mapDispatchCard)(CartCard)