import {saveCards, saveCurrentCard} from "./actions";

export const saveCartListOperation = (cards) => (dispatch) => {
    dispatch(saveCards(cards));
}

export const saveCurrentCardOperation = (card) => (dispatch) =>{
    dispatch(saveCurrentCard(card));
}
