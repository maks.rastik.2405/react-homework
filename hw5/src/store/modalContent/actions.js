import * as modalContentAction from './types'

export const contentBuy = (item) =>({
    type:modalContentAction.CHANGE_MODAL_CONTENT_BUY,
    payload:item
})

export const contentDelete = (item) => ({
    type:modalContentAction.CHANGE_MODAL_CONTENT_DELETE,
    payload: item
})

export const contentBuyFavorite =(item) =>({
    type:modalContentAction.CHANGE_MODAL_CONTENT_BUY_FAV,
    payload:item
})
