import {contentBuy, contentDelete, contentBuyFavorite } from "./actions";

export const contentBuyOperation = (item) => (dispatch) => {

   dispatch(contentBuy(item))
}

export  const contentDeleteOperation = (item) => (dispatch)=> {

   dispatch(contentDelete(item))

}

export  const contentBuyFavoriteOperation = (item) => (dispatch) => {

   dispatch(contentBuyFavorite(item))

}