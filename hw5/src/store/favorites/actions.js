import * as favoriteAction from './types'

export const saveFavorite = (favoriteList)=>({
    type:favoriteAction.GET_FAVORITE_LIST,
    payload:favoriteList
})

export const addToFavorites = (card) =>({
    type:favoriteAction.ADD_TO_FAVORITE_LIST,
    payload:card
})

export const removeToFavorites = (card) =>({
    type:favoriteAction.REMOVE_TO_FAVORITE_LIST,
    payload:card
})