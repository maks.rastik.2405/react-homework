import React, { useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import {connect} from "react-redux";
import HomePage from "./components/HomePage/HomePage";
import Header from "./components/Header/Header";
import ShoppingCartPage from './components/CartPage/CartPage';
import FavoritesPage from './components/FavoritesPage/FavoritesPage';
import Modal from "./components/Modal/Modal";
import FormPage from "./components/FormPage/FormPage";
import axios from "axios";
import {removeFavoritesOperation, saveFavoriteListOperation} from './store/favorites/operations';
import { addToCartList } from './store/Cart/actions';
import {addToCartListOperation, getCartListOperation, removeToCartListOperation} from './store/Cart/operations';
import { closeModalOperation } from './store/modal/operations';
import { contentBuyOperation } from './store/modalContent/operation';
import {saveCartListOperation} from "./store/card/operations";



function App({stateModal,localStorageGetFavorites, getCards, getBasketList}) {

    useEffect(() => {
        axios.get('/product.json')
            .then(response => {
                getCards(response.data);
            })
            .catch(error => {
                console.error(error);
            });

        const storedFavorites = JSON.parse(localStorage.getItem('favorite')) || [];
        localStorageGetFavorites(storedFavorites);

        const storedBasket = JSON.parse(localStorage.getItem('basketList')) || [];
        getBasketList(storedBasket)

    }, []);

    return (
        <div>
            <Router>
                <Header/>
                <Routes>
                    <Route path="/" element={<HomePage/>}/>
                    <Route path="/shopping-cart" element={<ShoppingCartPage/>}/>
                    <Route path="/favorites" element={<FavoritesPage/>}/>
                    <Route path="/form" element={<FormPage/>}/>
                </Routes>
            </Router>
            {stateModal && <Modal/>}
        </div>
    );
};

const mapStateToProps = ({ cartItems, modal, modalContent}) => ({
    cartItems:cartItems,
    stateModal:modal,
    newModalContent:modalContent
});


const mapDispatchApp = (dispatch) => ({

    storedCartItems : (cartItem) => dispatch(addToCartList(cartItem)),
    closeModalWindow : () => dispatch(closeModalOperation()),
    addToCurrentCart : (cartItem) => dispatch(addToCartListOperation(cartItem)),
    handleRemoveFromCart : (cartItem) => dispatch(removeToCartListOperation(cartItem)),
    handleRemoveFromFavorites : (cartItem) => dispatch(removeFavoritesOperation(cartItem)),
    newModalContent:  (cartItem) => dispatch(contentBuyOperation(cartItem)),
    localStorageGetFavorites : (favorite) => dispatch(saveFavoriteListOperation(favorite)),
    getCards :(cards)=> dispatch(saveCartListOperation(cards)),
    getBasketList :(cards) => dispatch(getCartListOperation(cards))

})

export default connect(mapStateToProps, mapDispatchApp)(App);

